package com.ccc.services;

import com.ccc.model.Department;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
public class DepartmentServiceTest {

    private DepartmentService departmentService;

    @Before
    public void setUp() throws Exception {
        departmentService = new DepartmentService();

    }

    @After
    public void tearDown() throws Exception {
        departmentService = null;
    }

    @Test
    public void testGetDepartmentById() throws Exception {
        System.out.println(departmentService.getDepartmentById(1));
    }

    @Test
    public void testGetAllDepartment() throws Exception {
        List<Department> departmentList = departmentService.getAllDepartment();
        for (Department department : departmentList){
            System.out.println(department);
        }
    }

    @Test
    public void testGetDepartmentByDevision() throws Exception {
        List<Department> departmentList = departmentService.getDepartmentByDevision("ฝ่ายการพยาบาล");
        for (Department department : departmentList){
            System.out.println(department);
        }
    }
}