package com.ccc.services;

import com.ccc.model.Department;
import com.ccc.model.Poll;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
public class PollServiceTest {

    private PollService pollService;
    private  DepartmentService departmentService;

    @Before
    public void setUp() throws Exception {
        pollService = new PollService();
        departmentService = new DepartmentService();
    }

    @After
    public void tearDown() throws Exception {
        pollService = null;
    }

    @Test
    public void testInsertPoll() throws Exception {
        Poll poll = new Poll();
        poll.setSex("ชาย");


        Department department = departmentService.getDepartmentById(1);
        poll.setDepartment(department);

        poll.setAge("");
        poll.setEducation("");
        poll.setCurrentjobstatus("");
        poll.setExperience("");
        poll.setJob_descript1(1);
        poll.setJob_descript2(1);
        poll.setJob_descript3(1);
        poll.setJob_descript4(1);
        poll.setJob_descript5(1);

        poll.setStable1(2);
        poll.setStable2(2);
        poll.setStable3(2);
        poll.setStable4(2);
        poll.setStable5(2);
        poll.setStable6(2);

        poll.setCoop1(1);
        poll.setCoop2(1);
        poll.setCoop3(1);
        poll.setCoop4(1);
        poll.setCoop5(1);

        poll.setHead1(1);
        poll.setHead2(1);
        poll.setHead3(1);
        poll.setHead4(1);
        poll.setHead5(1);
        poll.setHead6(1);
        poll.setHead7(1);
        poll.setHead8(1);

        poll.setWelfare1(5);
        poll.setWelfare2(5);
        poll.setWelfare3(5);
        poll.setWelfare4(5);
        poll.setWelfare5(5);
        poll.setWelfare6(5);

        poll.setPlace1(3);
        poll.setPlace2(3);
        poll.setPlace3(3);
        poll.setPlace4(3);
        poll.setPlace5(3);
        poll.setPlace6(3);
        poll.setPlace7(3);
        poll.setPlace8(3);
        poll.setPlace9(3);

        poll.setAttempt1(3);
        poll.setAttempt2(3);
        poll.setAttempt3(3);
        poll.setAttempt4(3);
        poll.setAttempt5(3);
        poll.setAttempt6(3);

        poll.setProud1(4);
        poll.setProud2(4);
        poll.setProud3(4);
        poll.setProud4(4);
        poll.setProud5(4);

        poll.setPart1(4);
        poll.setPart2(4);
        poll.setPart3(4);
        poll.setPart4(4);

        poll.setSuggestion("ทดสอบ นะครับ");

        pollService.insertPoll(poll);
    }
}