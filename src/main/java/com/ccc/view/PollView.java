package com.ccc.view;

import com.ccc.model.Department;
import com.ccc.model.Poll;
import com.ccc.services.DepartmentService;
import com.ccc.services.PollService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
@ManagedBean(name="pollView")
@ViewScoped
public class PollView {

    private Poll pollEntity;

    @ManagedProperty("#{pollService}")
    private PollService pollService;

    @ManagedProperty("#{departmentView}")
    private DepartmentView departmentView;

    @ManagedProperty("#{department}")
    private DepartmentService departmentService;

    @PostConstruct
    public void init() {
//        pollEntity = new Poll();
    }

    //    <f:event type="preRenderView" listener="#{pollView.startForm}" />
    public void startForm(){
//        System.out.println("Start Form");
//        initial value for slider
//        setNumber3(1);
        pollEntity = new Poll();
        int defaultSlider = -3;
        departmentView.setSelectedDevision("");
        pollEntity.setSex("ชาย");
        pollEntity.setAge("");
        pollEntity.setEducation("");
        pollEntity.setCurrentjobstatus("");
        pollEntity.setExperience("");
        departmentView.setSelectedDepartment(0);
        pollEntity.setJob_descript1(defaultSlider);
        pollEntity.setJob_descript2(defaultSlider);
        pollEntity.setJob_descript3(defaultSlider);
        pollEntity.setJob_descript4(defaultSlider);
        pollEntity.setJob_descript5(defaultSlider);
        pollEntity.setStable1(defaultSlider);
        pollEntity.setStable2(defaultSlider);
        pollEntity.setStable3(defaultSlider);
        pollEntity.setStable4(defaultSlider);
        pollEntity.setStable5(defaultSlider);
        pollEntity.setStable6(defaultSlider);
        pollEntity.setCoop1(defaultSlider);
        pollEntity.setCoop2(defaultSlider);
        pollEntity.setCoop3(defaultSlider);
        pollEntity.setCoop4(defaultSlider);
        pollEntity.setCoop5(defaultSlider);
        pollEntity.setHead1(defaultSlider);
        pollEntity.setHead2(defaultSlider);
        pollEntity.setHead3(defaultSlider);
        pollEntity.setHead4(defaultSlider);
        pollEntity.setHead5(defaultSlider);
        pollEntity.setHead6(defaultSlider);
        pollEntity.setHead7(defaultSlider);
        pollEntity.setHead8(defaultSlider);
        pollEntity.setWelfare1(defaultSlider);
        pollEntity.setWelfare2(defaultSlider);
        pollEntity.setWelfare3(defaultSlider);
        pollEntity.setWelfare4(defaultSlider);
        pollEntity.setWelfare5(defaultSlider);
        pollEntity.setWelfare6(defaultSlider);
        pollEntity.setPlace1(defaultSlider);
        pollEntity.setPlace2(defaultSlider);
        pollEntity.setPlace3(defaultSlider);
        pollEntity.setPlace4(defaultSlider);
        pollEntity.setPlace5(defaultSlider);
        pollEntity.setPlace6(defaultSlider);
        pollEntity.setPlace7(defaultSlider);
        pollEntity.setPlace8(defaultSlider);
        pollEntity.setPlace9(defaultSlider);
        pollEntity.setAttempt1(defaultSlider);
        pollEntity.setAttempt2(defaultSlider);
        pollEntity.setAttempt3(defaultSlider);
        pollEntity.setAttempt4(defaultSlider);
        pollEntity.setAttempt5(defaultSlider);
        pollEntity.setAttempt6(defaultSlider);
        pollEntity.setProud1(defaultSlider);
        pollEntity.setProud2(defaultSlider);
        pollEntity.setProud3(defaultSlider);
        pollEntity.setProud4(defaultSlider);
        pollEntity.setProud5(defaultSlider);
        pollEntity.setPart1(defaultSlider);
        pollEntity.setPart2(defaultSlider);
        pollEntity.setPart3(defaultSlider);
        pollEntity.setPart4(defaultSlider);
    }

    public String newPoll(){
//        System.out.println(pollEntity);
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        DepartmentService departmentServicesBean = (DepartmentService) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"department");
//
        Department department = departmentService.getDepartmentById(departmentView.getSelectedDepartment());
        pollEntity.setDepartment(department);
//
//        pollEntity.setSex(pollEntity.getSex());
        pollEntity.setJob_descript1(positiveNumber(pollEntity.getJob_descript1()));
        pollEntity.setJob_descript2(positiveNumber(pollEntity.getJob_descript2()));
        pollEntity.setJob_descript3(positiveNumber(pollEntity.getJob_descript3()));
        pollEntity.setJob_descript4(positiveNumber(pollEntity.getJob_descript4()));
        pollEntity.setJob_descript5(positiveNumber(pollEntity.getJob_descript5()));

        pollEntity.setStable1(positiveNumber(pollEntity.getStable1()));
        pollEntity.setStable2(positiveNumber(pollEntity.getStable2()));
        pollEntity.setStable3(positiveNumber(pollEntity.getStable3()));
        pollEntity.setStable4(positiveNumber(pollEntity.getStable4()));
        pollEntity.setStable5(positiveNumber(pollEntity.getStable5()));
        pollEntity.setStable6(positiveNumber(pollEntity.getStable6()));

        pollEntity.setCoop1(positiveNumber(pollEntity.getCoop1()));
        pollEntity.setCoop2(positiveNumber(pollEntity.getCoop2()));
        pollEntity.setCoop3(positiveNumber(pollEntity.getCoop3()));
        pollEntity.setCoop4(positiveNumber(pollEntity.getCoop4()));
        pollEntity.setCoop5(positiveNumber(pollEntity.getCoop5()));

        pollEntity.setHead1(positiveNumber(pollEntity.getHead1()));
        pollEntity.setHead2(positiveNumber(pollEntity.getHead2()));
        pollEntity.setHead3(positiveNumber(pollEntity.getHead3()));
        pollEntity.setHead4(positiveNumber(pollEntity.getHead4()));
        pollEntity.setHead5(positiveNumber(pollEntity.getHead5()));
        pollEntity.setHead6(positiveNumber(pollEntity.getHead6()));
        pollEntity.setHead7(positiveNumber(pollEntity.getHead7()));
        pollEntity.setHead8(positiveNumber(pollEntity.getHead8()));

        pollEntity.setWelfare1(positiveNumber(pollEntity.getWelfare1()));
        pollEntity.setWelfare2(positiveNumber(pollEntity.getWelfare2()));
        pollEntity.setWelfare3(positiveNumber(pollEntity.getWelfare3()));
        pollEntity.setWelfare4(positiveNumber(pollEntity.getWelfare4()));
        pollEntity.setWelfare5(positiveNumber(pollEntity.getWelfare5()));
        pollEntity.setWelfare6(positiveNumber(pollEntity.getWelfare6()));

        pollEntity.setPlace1(positiveNumber(pollEntity.getPlace1()));
        pollEntity.setPlace2(positiveNumber(pollEntity.getPlace2()));
        pollEntity.setPlace3(positiveNumber(pollEntity.getPlace3()));
        pollEntity.setPlace4(positiveNumber(pollEntity.getPlace4()));
        pollEntity.setPlace5(positiveNumber(pollEntity.getPlace5()));
        pollEntity.setPlace6(positiveNumber(pollEntity.getPlace6()));
        pollEntity.setPlace7(positiveNumber(pollEntity.getPlace7()));
        pollEntity.setPlace8(positiveNumber(pollEntity.getPlace8()));
        pollEntity.setPlace9(positiveNumber(pollEntity.getPlace9()));

        pollEntity.setAttempt1(positiveNumber(pollEntity.getAttempt1()));
        pollEntity.setAttempt2(positiveNumber(pollEntity.getAttempt2()));
        pollEntity.setAttempt3(positiveNumber(pollEntity.getAttempt3()));
        pollEntity.setAttempt4(positiveNumber(pollEntity.getAttempt4()));
        pollEntity.setAttempt5(positiveNumber(pollEntity.getAttempt5()));
        pollEntity.setAttempt6(positiveNumber(pollEntity.getAttempt6()));

        pollEntity.setProud1(positiveNumber(pollEntity.getProud1()));
        pollEntity.setProud2(positiveNumber(pollEntity.getProud2()));
        pollEntity.setProud3(positiveNumber(pollEntity.getProud3()));
        pollEntity.setProud4(positiveNumber(pollEntity.getProud4()));
        pollEntity.setProud5(positiveNumber(pollEntity.getProud5()));

        pollEntity.setPart1(positiveNumber(pollEntity.getPart1()));
        pollEntity.setPart2(positiveNumber(pollEntity.getPart2()));
        pollEntity.setPart3(positiveNumber(pollEntity.getPart3()));
        pollEntity.setPart4(positiveNumber(pollEntity.getPart4()));

        pollService.insertPoll(pollEntity);
        return "/thankyouPage.xhtml?faces-redirect=true";
    }

    public int positiveNumber(int number){
        number = Math.abs(number);
        return number;
    }
    public Poll getPollEntity() {
        return pollEntity;
    }

    public void setPollEntity(Poll pollEntity) {
        this.pollEntity = pollEntity;
    }

    public DepartmentView getDepartmentView() {
        return departmentView;
    }

    public void setDepartmentView(DepartmentView departmentView) {
        this.departmentView = departmentView;
    }

    public PollService getPollService() {
        return pollService;
    }

    public void setPollService(PollService pollService) {
        this.pollService = pollService;
    }

    public DepartmentService getDepartmentService() {
        return departmentService;
    }

    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }
}
