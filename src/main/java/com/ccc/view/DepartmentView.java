package com.ccc.view;

import com.ccc.model.Department;
import com.ccc.services.DepartmentService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
@ManagedBean(name="departmentView")
@ViewScoped
public class DepartmentView {

    private String selectedDevision;

    private int selectedDepartment;

    private List<Department> departmentList;

    private String tmptextDevision;

    public void onDevisionChange(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        DepartmentService departmentServicesBean = (DepartmentService) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"department");
        departmentList = departmentServicesBean.getDepartmentByDevision(selectedDevision);
//        System.out.println(departmentList);
        setTmptextDevision(getSelectedDevision());
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    public String getSelectedDevision() {
        return selectedDevision;
    }

    public void setSelectedDevision(String selectedDevision) {
        this.selectedDevision = selectedDevision;
    }

    public int getSelectedDepartment() {
        return selectedDepartment;
    }

    public void setSelectedDepartment(int selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    public String getTmptextDevision() {
        return tmptextDevision;
    }

    public void setTmptextDevision(String tmptextDevision) {
        this.tmptextDevision = tmptextDevision;
    }
}
