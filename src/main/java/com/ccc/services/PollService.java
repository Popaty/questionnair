package com.ccc.services;

import com.ccc.mappers.PollMapper;
import com.ccc.model.Poll;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */

@ManagedBean(name = "pollService")
@ApplicationScoped
public class PollService implements PollMapper {
//    @Override
//    public List<Poll> getAllPoll() {
//        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
//        try {
//            PollMapper pollMapper = sqlSession.getMapper(PollMapper.class);
//            return pollMapper.getAllPoll();
//        } finally {
//            sqlSession.close();
//        }
//    }

    @Override
    public int insertPoll(Poll poll) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            PollMapper pollMapper = sqlSession.getMapper(PollMapper.class);
            int effectrow = pollMapper.insertPoll(poll);
            sqlSession.commit();
            return effectrow;

        } finally {
            sqlSession.close();
        }
    }
}
