package com.ccc.services;

import com.ccc.mappers.DepartmentMapper;
import com.ccc.model.Department;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
@ManagedBean (name = "department")
@ApplicationScoped

public class DepartmentService implements DepartmentMapper {

    @Override
    public List<Department> getAllDepartment() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
            return departmentMapper.getAllDepartment();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Department getDepartmentById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
            return departmentMapper.getDepartmentById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Department> getDepartmentByDevision(String devision) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
            return departmentMapper.getDepartmentByDevision(devision);
        } finally {
            sqlSession.close();
        }
    }
}
