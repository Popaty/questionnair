package com.ccc.model;

import java.util.Map;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
public class Poll {
    private int id;
    private String sex;
    private Department department;
    private String age;
    private String education;
    private String currentjobstatus;
    private String experience;


    private int job_descript1;
    private int job_descript2;
    private int job_descript3;
    private int job_descript4;
    private int job_descript5;

    private int stable1;
    private int stable2;
    private int stable3;
    private int stable4;
    private int stable5;
    private int stable6;

    private int coop1;
    private int coop2;
    private int coop3;
    private int coop4;
    private int coop5;

    private int head1;
    private int head2;
    private int head3;
    private int head4;
    private int head5;
    private int head6;
    private int head7;
    private int head8;

    private int welfare1;
    private int welfare2;
    private int welfare3;
    private int welfare4;
    private int welfare5;
    private int welfare6;

    private int place1;
    private int place2;
    private int place3;
    private int place4;
    private int place5;
    private int place6;
    private int place7;
    private int place8;
    private int place9;

    private int attempt1;
    private int attempt2;
    private int attempt3;
    private int attempt4;
    private int attempt5;
    private int attempt6;

    private int proud1;
    private int proud2;
    private int proud3;
    private int proud4;
    private int proud5;

    private int part1;
    private int part2;
    private int part3;
    private int part4;

    private String suggestion;
    private String suggestionPart2;

    @Override
    public String toString() {
        return "Poll{" +
                "id=" + id +
                ", sex='" + sex + '\'' +
                ", department=" + department +
                ", age='" + age + '\'' +
                ", education='" + education + '\'' +
                ", currentjobstatus='" + currentjobstatus + '\'' +
                ", experience='" + experience + '\'' +
                ", job_descript1=" + job_descript1 +
                ", job_descript2=" + job_descript2 +
                ", job_descript3=" + job_descript3 +
                ", job_descript4=" + job_descript4 +
                ", job_descript5=" + job_descript5 +
                ", stable1=" + stable1 +
                ", stable2=" + stable2 +
                ", stable3=" + stable3 +
                ", stable4=" + stable4 +
                ", stable5=" + stable5 +
                ", stable6=" + stable6 +
                ", coop1=" + coop1 +
                ", coop2=" + coop2 +
                ", coop3=" + coop3 +
                ", coop4=" + coop4 +
                ", coop5=" + coop5 +
                ", head1=" + head1 +
                ", head2=" + head2 +
                ", head3=" + head3 +
                ", head4=" + head4 +
                ", head5=" + head5 +
                ", head6=" + head6 +
                ", head7=" + head7 +
                ", head8=" + head8 +
                ", welfare1=" + welfare1 +
                ", welfare2=" + welfare2 +
                ", welfare3=" + welfare3 +
                ", welfare4=" + welfare4 +
                ", welfare5=" + welfare5 +
                ", welfare6=" + welfare6 +
                ", place1=" + place1 +
                ", place2=" + place2 +
                ", place3=" + place3 +
                ", place4=" + place4 +
                ", place5=" + place5 +
                ", place6=" + place6 +
                ", place7=" + place7 +
                ", place8=" + place8 +
                ", place9=" + place9 +
                ", attempt1=" + attempt1 +
                ", attempt2=" + attempt2 +
                ", attempt3=" + attempt3 +
                ", attempt4=" + attempt4 +
                ", attempt5=" + attempt5 +
                ", attempt6=" + attempt6 +
                ", proud1=" + proud1 +
                ", proud2=" + proud2 +
                ", proud3=" + proud3 +
                ", proud4=" + proud4 +
                ", proud5=" + proud5 +
                ", part1=" + part1 +
                ", part2=" + part2 +
                ", part3=" + part3 +
                ", part4=" + part4 +
                ", suggestion='" + suggestion + '\'' +
                ", suggestionPart2='" + suggestionPart2 + '\'' +
                '}';
    }

    public String getSuggestionPart2() {
        return suggestionPart2;
    }

    public void setSuggestionPart2(String suggestionPart2) {
        this.suggestionPart2 = suggestionPart2;
    }

    public int getProud1() {
        return proud1;
    }

    public void setProud1(int proud1) {
        this.proud1 = (proud1);
    }

    public int getProud2() {
        return proud2;
    }

    public void setProud2(int proud2) {
        this.proud2 = (proud2);
    }

    public int getProud3() {
        return proud3;
    }

    public void setProud3(int proud3) {
        this.proud3 = (proud3);
    }

    public int getProud4() {
        return proud4;
    }

    public void setProud4(int proud4) {
        this.proud4 = (proud4);
    }

    public int getProud5() {
        return proud5;
    }

    public void setProud5(int proud5) {
        this.proud5 = (proud5);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getCurrentjobstatus() {
        return currentjobstatus;
    }

    public void setCurrentjobstatus(String currentjobstatus) {
        this.currentjobstatus = currentjobstatus;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public int getJob_descript1() {
        return job_descript1;
    }

    public void setJob_descript1(int job_descript1) {
        this.job_descript1 = (job_descript1);
    }

    public int getJob_descript2() {
        return job_descript2;
    }

    public void setJob_descript2(int job_descript2) {
        this.job_descript2 = (job_descript2);
    }

    public int getJob_descript3() {
        return job_descript3;
    }

    public void setJob_descript3(int job_descript3) {
        this.job_descript3 = (job_descript3);
    }

    public int getJob_descript4() {
        return job_descript4;
    }

    public void setJob_descript4(int job_descript4) {
        this.job_descript4 = (job_descript4);
    }

    public int getJob_descript5() {
        return job_descript5;
    }

    public void setJob_descript5(int job_descript5) {
        this.job_descript5 = (job_descript5)    ;
    }

    public int getStable1() {
        return stable1;
    }

    public void setStable1(int stable1) {
        this.stable1 = (stable1);
    }

    public int getStable2() {
        return stable2;
    }

    public void setStable2(int stable2) {
        this.stable2 = (stable2);
    }

    public int getStable3() {
        return stable3;
    }

    public void setStable3(int stable3) {
        this.stable3 = (stable3);
    }

    public int getStable4() {
        return stable4;
    }

    public void setStable4(int stable4) {
        this.stable4 = (stable4);
    }

    public int getStable5() {
        return stable5;
    }

    public void setStable5(int stable5) {
        this.stable5 = (stable5);
    }

    public int getStable6() {
        return stable6;
    }

    public void setStable6(int stable6) {
        this.stable6 = (stable6);
    }

    public int getCoop1() {
        return coop1;
    }

    public void setCoop1(int coop1) {
        this.coop1 = (coop1);
    }

    public int getCoop2() {
        return coop2;
    }

    public void setCoop2(int coop2) {
        this.coop2 = (coop2);
    }

    public int getCoop3() {
        return coop3;
    }

    public void setCoop3(int coop3) {
        this.coop3 = (coop3);
    }

    public int getCoop4() {
        return coop4;
    }

    public void setCoop4(int coop4) {
        this.coop4 = (coop4);
    }

    public int getCoop5() {
        return coop5;
    }

    public void setCoop5(int coop5) {
        this.coop5 = (coop5);
    }

    public int getHead1() {
        return head1;
    }

    public void setHead1(int head1) {
        this.head1 = (head1);
    }

    public int getHead2() {
        return head2;
    }

    public void setHead2(int head2) {
        this.head2 = (head2);
    }

    public int getHead3() {
        return head3;
    }

    public void setHead3(int head3) {
        this.head3 = (head3);
    }

    public int getHead4() {
        return head4;
    }

    public void setHead4(int head4) {
        this.head4 = (head4);
    }

    public int getHead5() {
        return head5;
    }

    public void setHead5(int head5) {
        this.head5 = (head5);
    }

    public int getHead6() {
        return head6;
    }

    public void setHead6(int head6) {
        this.head6 = (head6);
    }

    public int getHead7() {
        return head7;
    }

    public void setHead7(int head7) {
        this.head7 = (head7);
    }

    public int getHead8() {
        return head8;
    }

    public void setHead8(int head8) {
        this.head8 = (head8);
    }

    public int getWelfare1() {
        return welfare1;
    }

    public void setWelfare1(int welfare1) {
        this.welfare1 = (welfare1);
    }

    public int getWelfare2() {
        return welfare2;
    }

    public void setWelfare2(int welfare2) {
        this.welfare2 = (welfare2);
    }

    public int getWelfare3() {
        return welfare3;
    }

    public void setWelfare3(int welfare3) {
        this.welfare3 = (welfare3);
    }

    public int getWelfare4() {
        return welfare4;
    }

    public void setWelfare4(int welfare4) {
        this.welfare4 = (welfare4);
    }

    public int getWelfare5() {
        return welfare5;
    }

    public void setWelfare5(int welfare5) {
        this.welfare5 = (welfare5);
    }

    public int getWelfare6() {
        return welfare6;
    }

    public void setWelfare6(int welfare6) {
        this.welfare6 = (welfare6);
    }

    public int getPlace1() {
        return place1;
    }

    public void setPlace1(int place1) {
        this.place1 = (place1);
    }

    public int getPlace2() {
        return place2;
    }

    public void setPlace2(int place2) {
        this.place2 = (place2);
    }

    public int getPlace3() {
        return place3;
    }

    public void setPlace3(int place3) {
        this.place3 = (place3);
    }

    public int getPlace4() {
        return place4;
    }

    public void setPlace4(int place4) {
        this.place4 = (place4);
    }

    public int getPlace5() {
        return place5;
    }

    public void setPlace5(int place5) {
        this.place5 = (place5);
    }

    public int getPlace6() {
        return place6;
    }

    public void setPlace6(int place6) {
        this.place6 = (place6);
    }

    public int getPlace7() {
        return place7;
    }

    public void setPlace7(int place7) {
        this.place7 = (place7);
    }

    public int getPlace8() {
        return place8;
    }

    public void setPlace8(int place8) {
        this.place8 = (place8);
    }

    public int getPlace9() {
        return place9;
    }

    public void setPlace9(int place9) {
        this.place9 = (place9);
    }

    public int getAttempt1() {
        return attempt1;
    }

    public void setAttempt1(int attempt1) {
        this.attempt1 = (attempt1);
    }

    public int getAttempt2() {
        return attempt2;
    }

    public void setAttempt2(int attempt2) {
        this.attempt2 = (attempt2);
    }

    public int getAttempt3() {
        return attempt3;
    }

    public void setAttempt3(int attempt3) {
        this.attempt3 = (attempt3);
    }

    public int getAttempt4() {
        return attempt4;
    }

    public void setAttempt4(int attempt4) {
        this.attempt4 = (attempt4);
    }

    public int getAttempt5() {
        return attempt5;
    }

    public void setAttempt5(int attempt5) {
        this.attempt5 = (attempt5);
    }

    public int getAttempt6() {
        return attempt6;
    }

    public void setAttempt6(int attempt6) {
        this.attempt6 = (attempt6);
    }


    public int getPart1() {
        return part1;
    }

    public void setPart1(int part1) {
        this.part1 = (part1);
    }

    public int getPart2() {
        return part2;
    }

    public void setPart2(int part2) {
        this.part2 = (part2);
    }

    public int getPart3() {
        return part3;
    }

    public void setPart3(int part3) {
        this.part3 = (part3);
    }

    public int getPart4() {
        return part4;
    }

    public void setPart4(int part4) {
        this.part4 = (part4);
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }
}
