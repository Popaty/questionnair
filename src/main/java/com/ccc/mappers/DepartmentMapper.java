package com.ccc.mappers;

import com.ccc.model.Department;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
public interface DepartmentMapper {

    @Select("select * from department")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "departmentName", column = "department_name"),
    })
    public List<Department> getAllDepartment();

    @Select("select * from department where id = #{id}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "departmentName", column = "department_name"),
    })
    public Department getDepartmentById(int id);

    @Select("select * from department where devision = #{param1}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "departmentName", column = "department_name"),
    })
    public List<Department> getDepartmentByDevision(String devision);
}
