package com.ccc.mappers;

import com.ccc.model.Poll;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Maximiliano on 10/26/2015 AD.
 */
public interface PollMapper {

//    @Select("select * from poll")
//    @Results({
//            @Result(id = true, property = "id", column = "id"),
//            @Result(property = "firstName", column = "first_name"),
//            @Result(property = "department", column = "department_id",
//                    one = @One(select = "com.skoode.mappers.DepartmentMapper.getDepartmentById"))
//    })
//    public List<Poll> getAllPoll();

    @Insert("insert into poll(sex, department, age, education, currentjobstatus, experience, " +
            "job_descript1, job_descript2, job_descript3, job_descript4, job_descript5, stable1, " +
            "stable2, stable3, stable4, stable5, stable6, coop1, coop2, coop3, coop4, coop5, " +
            "head1, head2, head3, head4, head5, head6, head7, head8, welfare1, welfare2, welfare3, " +
            "welfare4, welfare5, welfare6, place1, place2, place3, place4, place5, place6, place7, " +
            "place8, place9, attempt1, attempt2, attempt3, attempt4, attempt5, attempt6, proud1, " +
            "proud2, proud3, proud4, proud5, part1, part2, part3, part4, suggestion, suggestion_part2)" +
            "values(#{sex}, #{department.id}, #{age}, #{education}, #{currentjobstatus}, " +
            "#{experience}, #{job_descript1}, #{job_descript2}, #{job_descript3}, " +
            "#{job_descript4}, #{job_descript5}, #{stable1}, #{stable2}, #{stable3}, #{stable4}, " +
            "#{stable5}, #{stable6}, #{coop1}, #{coop2}, #{coop3}, #{coop4}, #{coop5}, #{head1}, " +
            "#{head2}, #{head3}, #{head4}, #{head5}, #{head6}, #{head7}, #{head8}, #{welfare1}, " +
            "#{welfare2}, #{welfare3}, #{welfare4}, #{welfare5}, #{welfare6}, #{place1}, " +
            "#{place2}, #{place3}, #{place4}, #{place5}, #{place6}, #{place7}, #{place8}, " +
            "#{place9}, #{attempt1}, #{attempt2}, #{attempt3}, #{attempt4}, #{attempt5}, " +
            "#{attempt6}, #{proud1}, #{proud2}, #{proud3}, #{proud4}, #{proud5}, #{part1}, " +
            "#{part2}, #{part3}, #{part4}, #{suggestion}, #{suggestionPart2})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insertPoll(Poll poll);

}
